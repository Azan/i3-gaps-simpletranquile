#!/usr/bin/env bash

if [ -d ~/.config/i3 ]; then
	echo -n "creating existing i3 config backup... "
	cp ~/.config/i3/config ~/.config/i3/config.bak
	if [ -e ~/.config/i3/config.bak ]; then
		echo -e "[ \x1b[32mdone\x1b[0m ]"
	else
		echo -e "[ \x1b[31mfail\x1b[0m ]"
		exit 255
	fi
	echo -n "copying i3-gaps-simpletranquile i3 config... "
	cp ./i3/config ~/.config/i3/
	echo -e "[ \x1b[32mdone\x1b[0m ]"	
else
	mkdir -p ~/.config/i3
	echo -n "copying i3-gaps-simpletranquile i3 config... "
	cp ./i3/config ~/.config/i3/
	if [ -e ~/.config/i3/config ]; then
		echo -e "[ \x1b[32mdone\x1b[0m ]"
	else
		echo -e "[ \x1b[31mfail\x1b[0m ]"
		exit 255
	fi
fi

if [ -d ~/.config/polybar ]; then
	echo -n "creating existing polybar config backup... "
	cp ~/.config/polybar/config ~/.config/polybar/config.bak
	if [ -e ~/.config/polybar/config.bak ]; then
		echo -e "[ \x1b[32mdone\x1b[0m ]"
	else
		echo -e "[ \x1b[31mfail\x1b[0m ]"
		exit 255
	fi
	echo -n "copying i3-gaps-simpletranquile polybar config... "
	cp ./polybar/config ~/.config/polybar/
	echo -e "[ \x1b[32mdone\x1b[0m ]"	
else
	mkdir -p ~/.config/polybar
	echo -n "copying i3-gaps-simpletranquile polybar config... "
	cp ./polybar/config ~/.config/polybar/
	if [ -e ~/.config/polybar/config ]; then
		echo -e "[ \x1b[32mdone\x1b[0m ]"
	else
		echo -e "[ \x1b[31mfail\x1b[0m ]"
		exit 255
	fi
fi

echo -e "[ \x1b[32mall done\x1b[0m ]"
exit 0